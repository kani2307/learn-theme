package com.example.learntheme

import android.app.UiModeManager
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.RequiresApi
import androidx.compose.foundation.background
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.Button
import androidx.compose.material.Switch
import androidx.compose.material.Text
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.modifier.modifierLocalConsumer
import androidx.compose.ui.unit.dp
import com.example.expense.ui.theme.ExpenseTheme

class SettingPage:ComponentActivity() {
    val context = this
    @RequiresApi(Build.VERSION_CODES.S)
    @Override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent{
            ExpenseTheme {
                var isNghtMode = isSystemInDarkTheme()
                var checkValue = remember { mutableStateOf(isNghtMode) }
                val uiModeManager = getSystemService(UI_MODE_SERVICE) as UiModeManager
                Column(modifier = Modifier.padding(start = 20.dp)) {
                    Button(onClick = { val intent = Intent(context, HomePage::class.java)
                        startActivity(intent)}) {
                        Text(text = "Back button", color = if(isSystemInDarkTheme()){Color.White}else{
                            Color.Black
                        } )

                    }
                    Row {
                        Text(text = "Theme button",color = if(isSystemInDarkTheme()){Color.White}else{
                            Color.Black

                        } )
                        Spacer(modifier = Modifier.width(50.dp))





                        Switch(checked = checkValue.value, onCheckedChange = {

                            if (checkValue.value) {
                                Log.e("night-->",uiModeManager.nightMode.toString())
                                checkValue .value= false
                                uiModeManager.setApplicationNightMode(UiModeManager.MODE_NIGHT_NO)

                                Log.e("count","1")

                            } else {
                                Log.e("night-->",uiModeManager.nightMode.toString())
                                checkValue.value = true
                                uiModeManager.setApplicationNightMode(UiModeManager.MODE_NIGHT_YES)

                                Log.e("count","2")


                            }




                        },modifier = Modifier.offset(y=-12.dp))
                        
                    }
                    


                }

            }
    }
}
}