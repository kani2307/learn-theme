package com.example.expense.ui.theme

import androidx.compose.ui.graphics.Color

val primary_dark = Color(0xFFFFFFFF)
val secondary_dark = Color(0xFFCCC2DC)
val tertiary_dark = Color(0xFF6D6D6D)
val surface_dark = Color(0xFF151515)
val on_primary_dark = Color(0xFFFFFFFF)
val on_surface_variant_dark =  Color(0xFF909090)


val primary_light = Color(0xFF151515)
val secondary_light = Color(0xFF625b71)
val  tertiary_light = Color(0xFF6D6D6D)
val surface_light = Color(0xFFFFFFFF)
val on_primary_light = Color(0xFF000000)
val on_surface_variant_light =  Color(0xFF909090)