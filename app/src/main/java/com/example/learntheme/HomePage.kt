package com.example.learntheme

import android.app.UiModeManager
import android.content.Context
import android.content.Context.*
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.RequiresApi
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Tab
//import androidx.compose.material3.TabBaselineLayout
import androidx.compose.material3.TabPosition
import androidx.compose.material3.TabRow
import androidx.compose.material3.Text
//import androidx.compose.material3.fromToken
//import androidx.compose.material3.tokens.PrimaryNavigationTabTokens
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.example.expense.ui.theme.ExpenseTheme
import com.example.expense.ui.theme.*
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.rememberPagerState
import com.google.android.material.tabs.TabLayout.Tab

class HomePage:ComponentActivity() {


    val context: Context = this

    @RequiresApi(Build.VERSION_CODES.S)
    @Override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent{
            ExpenseTheme {

                Column {
                    setTopBar(context)
                    setTabs()
                }

            }
        }

    }
    @RequiresApi(Build.VERSION_CODES.S)
    @Composable
    fun setTopBar(context: Context){



        Box(modifier = Modifier
            .fillMaxWidth(1f)
            .height(60.dp)
            .padding(start = 20.dp, end = 20.dp, top = 10.dp, bottom = 10.dp)

        ){
            Row(modifier = Modifier.fillMaxSize(), horizontalArrangement  =  Arrangement.SpaceBetween){
                Image(painter = painterResource(id = R.drawable.user_original ),
                    contentDescription = null,
                    modifier = Modifier
                        .width(40.dp)
                        .height(40.dp)
                        .clip(CircleShape)
                        .clickable {
                            val intent = Intent(context, SettingPage::class.java)
                            startActivity(intent)
                        }
                )
                Row {
                    Image(painter = painterResource(id = if(isSystemInDarkTheme()){R.drawable.notification_white}else{R.drawable.notification_black} ),
                        contentDescription = null,
                        modifier = Modifier
                            .width(40.dp)
                            .height(40.dp)
                            .clip(CircleShape)
                    )
                    Spacer(modifier = Modifier.width(20.dp))
                    Image(painter = painterResource(id = if(isSystemInDarkTheme()){R.drawable.home_search}else{R.drawable.home_search_black} ),
                        contentDescription = null,
                        modifier = Modifier
                            .width(40.dp)
                            .height(40.dp)
                            .clip(CircleShape)
                    )
                }
            }
        }

    }
    @OptIn(ExperimentalPagerApi::class)
    @Composable
    fun CustomIndicator(tabPosition: List<TabPosition>,index:Int){
        val width  = tabPosition[index].width
        val offsetX = tabPosition[index].left
        Box(modifier = Modifier
            .fillMaxSize()
            .wrapContentSize(align = Alignment.BottomStart)
            .offset(offsetX)
            .width(width)
            .padding(4.dp)
            .border(BorderStroke(2.dp, Color.Red))


        )
    }

    @OptIn(ExperimentalPagerApi::class)
    @Composable
    fun setTabs(){
        var selectedTabIndex = remember {
            mutableIntStateOf(0)
        }
        var pagerState = rememberPagerState(pageCount = 3)
        val tabItems = listOf(
            TabItems("Favourites"),
            TabItems("Workspace"),
            TabItems("Dashboard")
        )
        LaunchedEffect(selectedTabIndex.value){
            pagerState.animateScrollToPage(selectedTabIndex.value)
        }
        LaunchedEffect(pagerState.currentPage){
            selectedTabIndex.value = pagerState.currentPage
        }

        TabRow(selectedTabIndex = selectedTabIndex.value,
//            indicator = {tabPositions ->  CustomIndicator(tabPositions,selectedTabIndex.value)}
        ){
            tabItems.forEachIndexed { index, tabItems ->
                Tab(selected = index == selectedTabIndex.value, onClick = {
                    selectedTabIndex.value = index
                }) {
                    Text(
                        text = tabItems.title, color =
                                if (index == selectedTabIndex.value) {
                                    MaterialTheme.colorScheme.onPrimary
                                } else {
                                    MaterialTheme.colorScheme.onSurfaceVariant
                                }
                    )
                    Spacer(modifier = Modifier.height(10.dp))
                }

            }
        }
        HorizontalPager(state = pagerState,
            modifier = Modifier
                .fillMaxSize()
        ) {index ->
            Box(modifier = Modifier.fillMaxSize()
            ){
                Text(text = tabItems[index].title, color= if(isSystemInDarkTheme()){
                    Color.White
                }else{
                    Color.Black
                })
            }
        }

    }




}



data class TabItems(
    val title : String
)
